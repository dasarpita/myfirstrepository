#!/bin/bash

XVFB_OUTPUT=/tmp/Xvfb.out
XVFB=$2
XVFB_OPTIONS=":$3 -screen 0 $4"

#Start Xvfb Display
start_xvfb()
{
	#echo -n "Starting : X Virtual Frame Buffer "
	$XVFB $XVFB_OPTIONS >>$XVFB_OUTPUT 2>&1&
}

case "$1" in
	start_xvfb)
		start_xvfb
	;;	

	*)
		echo "Usage: xvfb {start_xvfb}"
	exit 1
	;;
esac
exit $?
