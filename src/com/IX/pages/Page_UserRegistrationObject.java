package com.IX.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class Page_UserRegistrationObject {
	
	
	@FindBy(id="email")
	WebElement txt_EmailAddress;
	
	@FindBy(id="password")
	WebElement txt_Password;
	
	@FindBy(id ="confirm_password")
	WebElement txt_ConfirmPassword;
	
	@FindBy(id ="firstname")
	WebElement txt_FirstName;
	
	@FindBy(id ="lastname")
	WebElement txt_LastName;
	
	@FindBy(id ="role")
	WebElement drop_SelectRole;
	
	@FindBy(id ="is_planner")
	WebElement check_IsOldPlanner;
	
	@FindBy(id ="is_placements")
	WebElement check_IsPlacementsInReport;
	
	@FindBy(id ="is_screenshots")
	WebElement check_IsScreenshotsInReport;
	
	@FindBy(id ="is_engagements")
	WebElement check_IsEngagementsInReport;
	
	@FindBy(id ="copy_paste_credentials")
	WebElement redio_CopyPasteCredentials;
	
	@FindBy(id ="automatically_send_email")
	WebElement redio_SendEmail;
	
	@FindBy (id ="address_1")
	WebElement txt_Address1;
	
	@FindBy (id ="address_2")
	WebElement txt_Address2;
	
	@FindBy (id ="city")
	WebElement txt_City;
	
	@FindBy (id ="state")
	WebElement txt_State;
	
	@FindBy (id ="zip")
	WebElement txt_ZipCode;
	
	@FindBy (id ="phone_number")
	WebElement txt_PhoneNumber;
	
	@FindBy (id ="isGroupSuper")
	WebElement check_IsGroupSuper;
	@FindBy (xpath=".//*[@id='s2id_advertiser_select']/a")
	WebElement drop_AdvertiserOrg;
	
	@FindBy (xpath=".//*[@id='select2-drop']/div/input")
	WebElement txt_DropFocused;
	
	@FindBy(xpath ="//*[@id='s2id_partner_select']/a")
	WebElement drop_Partner;
	
	
	@FindBy (xpath=".//*[@id='submit_button']/input")
	WebElement btn_Register;
	
	@FindBy (xpath ="//div[@class='alert alert-success']")
	WebElement msg_Success;

	

	
	public WebElement select_ValueFromDropdownForRegistration(
			EventFiringWebDriver driver, String dropVal) {
		String autoDropdownXpath = "//*[@id='select2-drop']/ul/li/div/span[text()='*String*']";
		WebElement webElement = driver.findElement(By.xpath(autoDropdownXpath
				.replace("*String*", dropVal)));
		return webElement;
	}
	
	
	
}
