package com.IX.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.IX.framework.Driver;
import com.IX.utilities.FrequenceSeleniumConfiguration;
import com.IX.utilities.GenericServiceController;

public class Page_RoleBaseLogins extends Driver {

	Page_LoginObject page_LoginObject;
	GenericServiceController genericServiceController = new GenericServiceController();
	
	
	/**
	 * This is common login method which simply takes user name, passwords and
	 * application url and check login action.
	 * 
	 * @param userName
	 * @param password
	 */
	private void userLogin(final String strUserName, final String strPassword) {
		System.out.println("Driver:" + driver);
		page_LoginObject = new Page_LoginObject(driver);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		page_LoginObject.Txtbox_User_Email.sendKeys(strUserName);
		page_LoginObject.Txtbox_User_Password.sendKeys(strPassword);
		page_LoginObject.Btn_SignIn.click();
		//Assert.assertEquals(driver.getTitle(), "AL4k [Campaigns]", "Username or password is wrong. Please check");
		}
	
	/**
	   * This is login method for Admin user role.
	*/
	public void testAdminUserLogin() {
		userLogin(FrequenceSeleniumConfiguration.getConfigConstants().getTestAdminUserName(), 
				FrequenceSeleniumConfiguration.getConfigConstants().getTestAdminPassword());
	}

	/**
	   * This is login method for Admin user role.
	*/
	public void testInvalidAdminLogin() {
		//userLogin(EngageSeleniumConfiguration.getConfigConstants().getTestAdminUserName(), 
	//			EngageSeleniumConfiguration.getConfigConstants().getTestAdminPassword());
	
	}
	
	/**
	   * This is login method for Sales user role.
	*/
	public void salesUserLogin() {
		userLogin(FrequenceSeleniumConfiguration.getConfigConstants().getSalesUserName(), 
				FrequenceSeleniumConfiguration.getConfigConstants().getSalesPassword());
	}
	
	/**
	   * This is login method for Ops user role.
	*/
	public void opsUserLogin() {
		userLogin(FrequenceSeleniumConfiguration.getConfigConstants().getOpsUserName(), 
				FrequenceSeleniumConfiguration.getConfigConstants().getOpsUserName());
	}
	
	/**
	   * This is login method for Advertiser user role.
	*/
	public void advertiserUserLogin() {
		userLogin(FrequenceSeleniumConfiguration.getConfigConstants().getAdvertiserUserName(), 
				FrequenceSeleniumConfiguration.getConfigConstants().getAdvertiserPassword());
	}
	/**
	 * This is logout method.
	 * 
	 */
	public void logout() {
		page_LoginObject.Dropdown_Toggle.click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.elementToBeClickable(page_LoginObject.Btn_LogOut));
		page_LoginObject.Btn_LogOut.click();
		
	}
	
	public void logoutNewFlow(){
		
		//WebDriverWait wait = new WebDriverWait(driver, 120);
		
		//wait.until(ExpectedConditions.elementToBeClickable(page_LoginObject.NewFlow_logout));
		page_LoginObject.NewFlow_logout.click();
		
		//genericServiceController.clickOnElementUsingJavascriptExecutor(driver, page_LoginObject.Link_Logout);
		
		((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView(true);", page_LoginObject.Link_Logout);
	    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", page_LoginObject.Link_Logout);

		
	}
	
}
