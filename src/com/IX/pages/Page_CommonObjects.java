package com.IX.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page_CommonObjects {

	@FindBy(xpath = "//*[@class='pull_nav_toggle']")
	public WebElement drop_navToggle;

	@FindBy(linkText = "Proposals")
	public WebElement link_navProposal;

	@FindBy(linkText = "Register")
	public WebElement btn_register;

	/* Log off button on a user logged-in screen */
	@FindBy(css = ".btn.btn-inverse.dropdown-toggle")
	public WebElement drop_toggle;

	/* Log off button on a user logged-in screen */
	@FindBy(xpath = ".//*[@id='pv_rfp_link_button']")
	public WebElement link_newProposal;

	/* Go to creatives request link */
	@FindBy(xpath = "//*[@id='banner_intake_feature_link']")
	public WebElement link_Creatives;
	
	/* Go to access manager link */
	@FindBy(linkText = "Access Manager")
	public WebElement btn_access;
	
	@FindBy(xpath="//a[contains(@href,'/access_manager')]")
    public WebElement link_navAccessManager;
	
	/* Go to AdvertiserEdit link */
    @FindBy(xpath="//a[contains(@href,'/advertisers')]")
    public WebElement link_advertisereditor;
    
    /* Go to adgroup link */
	@FindBy(xpath=".//*[@id='adgroups_menu']/a")
    public WebElement menu_adgroups;
	
	/* Go to IO link */
	@FindBy(xpath=".//*[@id='pull_io_feature_link']/a/span")
	public WebElement link_navInsertionOrder;
	
	/* Go to Create Partner link */
	@FindBy(linkText = "Partners")
	public WebElement link_createpartner;
	
	/* Go to New Partner link */
	@FindBy(linkText = "New Partner")
	public WebElement link_newpartner;
	
	/* Go to Report link */
	@FindBy(linkText = "Reports")
	public WebElement link_report;
	
	
	@FindBy (css= "#io_feature_link>a")
	public WebElement link_navInsertionOrderNewFlow;
	
	
	@FindBy(xpath = "//*[@aria-hidden='false']//*[@class='picker__select--year browser-default']")
	public WebElement drop_presentationYear;
	
	@FindBy(xpath = "//*[@aria-hidden='false']//*[@class='picker__select--month browser-default']")
	public WebElement drop_presentationMonth;
	
	@FindBy(xpath = ".//*[@class='pv_modal_column_container']/div[@class='pv_opportunity_name pv_tooltip']")
	public List<WebElement> list_proposals;
	
	@FindBy(xpath = ".//*[@id='all_ios_table']//div[position()=1]/small")
	public List<WebElement> list_IOs;
	
	@FindBy(xpath = ".//*[@id='all_ios_table']")
	public WebElement list_IO;
 
	@FindBy(xpath = ".//*[@id='vl_header_partner_img_element']")
	public WebElement frequence_Logo;
	
	@FindBy(linkText = "Campaigns")
	public WebElement link_campaignlist;
	
	
	public WebElement select_Value_From_AutoDropdown(
			EventFiringWebDriver driver, String dropVal) {
		String autoDropdownXpath = ".//*[@id='select2-drop']/ul/li/div[text()='*String*']";
		WebElement webElement = driver.findElement(By.xpath(autoDropdownXpath
				.replace("*String*", dropVal)));
		return webElement;
	}
	
	public WebElement select_Value_From_Dropdown(EventFiringWebDriver driver,
			String dropVal) {

		String dropdownXpath = ".//ul[starts-with(@class,'dropdown-content select-dropdown active')]/*[.='*String*']";

		WebElement webElement = driver.findElement(By.xpath(dropdownXpath
				.replace("*String*", dropVal)));
		
		return webElement;
	}
	
	/**
	   * Create Proposal page: This method is to select Year from year dropdown
	   * picket widget.
	   * 
	   * @param driver
	   * @param Date
	   *          : input date string value to be selected from widget.
	   * @return
	  */
	  public static WebElement Select_Date_From_DatePicker(EventFiringWebDriver driver, String strDate) {
	    String dateXpath = "//tbody/tr/td/div[text()='*dateString*' and @class='picker__day picker__day--infocus']";
	    
	    WebElement webElement =
	        driver.findElement(By.xpath(dateXpath.replace("*dateString*", strDate)));
	    return webElement;
	  }
	  
	  public static WebElement Select_Date_From_DatePickerOnIO(EventFiringWebDriver driver, String strDate) {
		    String dateXpath = ".//div[@aria-hidden='false']//tbody/tr/td/div[@class='picker__day picker__day--infocus' and text()='*dateString*']";
		    WebElement webElement = null;
		    try {
		    System.out.println("in function" + dateXpath.replace("*dateString*", strDate));
		    webElement =
		        driver.findElement(By.xpath(dateXpath.replace("*dateString*", strDate)));
		    }
		    catch(Exception e) {
		    	System.out.println("in exce");
		    	System.out.println(e);
		    }
		    return webElement;
		    
	}
	  
}
