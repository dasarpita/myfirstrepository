package com.IX.pages;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import com.IX.dataobject.HomePage_FieldNames;
import com.IX.framework.Driver;
import com.IX.utilities.GenericServiceController;

public class HomePage_Page extends Driver {
	
	HomePage_PageObject homePage_PageObject;
	Page_LoginObject page_LoginObject;
	Page_CommonObjects page_CommonObjects;
	
	GenericServiceController genericServiceController;
	
	public HomePage_Page (EventFiringWebDriver driver) 
	
	{
		// TODO Auto-generated constructor stub
		this.driver = driver;
		homePage_PageObject = PageFactory.initElements(driver, HomePage_PageObject.class);
		page_LoginObject = new Page_LoginObject(driver);
		page_CommonObjects = PageFactory.initElements(driver, Page_CommonObjects.class);
		genericServiceController = new GenericServiceController(driver);
				
	}	
	
	public String verifyPage(HomePage_FieldNames homePage_FieldNames,String userEmailAddress) throws InterruptedException{
		
		System.out.println("==================================================================================");
		System.out.println("==================================================================================");
		
		if (homePage_PageObject.btn_IX.isDisplayed())
		{
			System.out.println("IX button is present on Page");
			homePage_PageObject.btn_IX.click();
			System.out.println(driver.getTitle());
			System.out.println(driver.getCurrentUrl());
			System.out.println("==================================================================================");
			
		}	
		else 
		{
			System.out.println("IX button is NOT present on Page");
		}	
		
		if (homePage_PageObject.btn_ContactUs.isDisplayed())
		{
			System.out.println("Contact Us button is present on Page");
			homePage_PageObject.btn_ContactUs.click();
			Thread.sleep(3000);
			System.out.println(driver.getTitle());
			System.out.println(driver.getCurrentUrl());
			
			homePage_PageObject.btn_IX.click();
			Thread.sleep(3000);
			System.out.println(driver.getTitle());
			System.out.println(driver.getCurrentUrl());
			
			
			System.out.println("==================================================================================");
			
		}	
		else 
		{
			System.out.println("Contact Us is NOT present on Page");
		}	
				
		if (homePage_PageObject.btn_Nav.isDisplayed())
		{
			System.out.println("Nav is present on Page");
			homePage_PageObject.btn_Nav.click();
			System.out.println(homePage_PageObject.btn_Nav.getText());
			homePage_PageObject.btn_Nav.click();
			System.out.println("==================================================================================");
		}	
		else 
		{
			System.out.println("Nav is NOT present on Page");
		}
		
		if (homePage_PageObject.btn_WatchOurReel.isDisplayed())
		{
			System.out.println(homePage_PageObject.btn_WatchOurReel.getText() + " " + " button is present on Page");
			homePage_PageObject.btn_WatchOurReel.click();
			Thread.sleep(3000);
			System.out.println(driver.getTitle());
			System.out.println(driver.getCurrentUrl());
			
		}	
		else 
		{
			System.out.println(homePage_PageObject.btn_WatchOurReel.getText() + " " + " button is NOT present on Page");
		}
		
		if (homePage_PageObject.btn_LatestWork.isDisplayed())
		{
			System.out.println(homePage_PageObject.btn_LatestWork.getText() + " " + " button is present on Page");
		}	
		else 
		{
			System.out.println(homePage_PageObject.btn_LatestWork.getText() + " " + " button is NOT present on Page");
		}
		
		if (homePage_PageObject.link_ToWatch.isDisplayed())
		{
			System.out.println(homePage_PageObject.link_ToWatch.getText() + "" + "Link is present on Page");
		}	
		else 
		{
			System.out.println(homePage_PageObject.link_ToWatch.getText() + "" + "Link is NOT present on Page");
		}
		
		if (homePage_PageObject.link_AgenciesToWatch.isDisplayed())
		{
			System.out.println(homePage_PageObject.link_AgenciesToWatch.getText() + " " + " Link is present on Page");
		}	
		else 
		{
			System.out.println(homePage_PageObject.link_AgenciesToWatch.getText() + " " + " Link is NOT present on Page");
		}		
		
		if (homePage_PageObject.section_FeaturedWork.isDisplayed())
		{
			System.out.println(homePage_PageObject.section_FeaturedWork.getText() + " " + " Section is present on Page");
		}	
		else 
		{
			System.out.println(homePage_PageObject.section_FeaturedWork.getText() + " " + " Section is NOT present on Page");
		}
		
		if (homePage_PageObject.link_Previous.isDisplayed())
		{
			System.out.println(homePage_PageObject.link_Previous.getText() + " " + " link is present on Page");
		}	
		else 
		{
			System.out.println(homePage_PageObject.link_Previous.getText() + " " + " link is NOT present on Page");
		}
		
		if (homePage_PageObject.link_Next.isDisplayed())
		{
			System.out.println(homePage_PageObject.link_Next.getText() + " " + " link is present on Page");
		}	
		else 
		{
			System.out.println(homePage_PageObject.link_Next.getText() + " " + " link is NOT present on Page");
		}
		
		if (homePage_PageObject.tile_1.isEnabled())
		{
			System.out.println(homePage_PageObject.tile_txt.getText() + " " + " Tile is present on Page");
		}	
		else 
		{
			System.out.println(homePage_PageObject.tile_1.getText() + " " + " Tile is NOT present on Page");
		}
				
		if (homePage_PageObject.Radiobtn_1.isDisplayed())
		{
			System.out.println("Carousel radio buttons are present on Page");
		}	
		else 
		{
			System.out.println("Carousel radio buttons are NOT present on Page");
		}
		
		if (homePage_PageObject.btn_ViewMoreWork.isDisplayed())
		{
			System.out.println(homePage_PageObject.btn_ViewMoreWork.getText() + " " + " link is present on Page");
		}	
		else 
		{
			System.out.println(homePage_PageObject.btn_ViewMoreWork.getText() + " " + " link is NOT present on Page");
		}
				
		System.out.println("==================================================================================");
		System.out.println("==================================================================================");
		
		if (homePage_PageObject.section_Footer.isDisplayed())
		{
			System.out.println(homePage_PageObject.section_Footer.getText() + " " + " section is present on Page");
		}	
		else 
		{
			System.out.println(homePage_PageObject.section_Footer.getText() + " " + " section is NOT present on Page");
		}
				
		System.out.println("==================================================================================");
		System.out.println("==================================================================================");
		
		if (homePage_PageObject.section_FollowUs.isDisplayed())
		{
			System.out.println(homePage_PageObject.section_FollowUs.getText() + " " + " section is present on Page");
		}	
		else 
		{
			System.out.println(homePage_PageObject.section_FollowUs.getText() + " " + " section is NOT present on Page");
		}
	
		if (homePage_PageObject.btn_Linkedin.isDisplayed())
		{
			System.out.println("LINKEDIN Icon is present on Page");
		}	
		else 
		{
			System.out.println("LINKEDIN Icon is NOT present on Page");
		}
		
		if (homePage_PageObject.btn_Facebook.isDisplayed())
		{
			System.out.println("FACEBOOK Icon is present on Page");
		}	
		else 
		{
			System.out.println("FACEBOOK Icon is NOT present on Page");
		}
		
		if (homePage_PageObject.btn_Instagram.isDisplayed())
		{
			System.out.println("INSTAGRAM Icon is present on Page");
		}	
		else 
		{
			System.out.println("INSTAGRAM Icon is NOT present on Page");
		}
		
		if (homePage_PageObject.btn_Twitter.isDisplayed())
		{
			System.out.println("TWITTER Icon is present on Page");
		}	
		else 
		{
			System.out.println("TWITTER Icon is NOT present on Page");
		}
		
		if (homePage_PageObject.btn_FooterIX.isDisplayed())
		{
			System.out.println("Footer IX Icon is present on Page");
		}	
		else 
		{
			System.out.println("Footer IX  Icon is NOT present on Page");
		}
		
		System.out.println("==================================================================================");
		System.out.println("==================================================================================");
		
		if (homePage_PageObject.dropdown_Region.isEnabled())
		{
			System.out.println(homePage_PageObject.dropdown_Region.getText()+" " + "Region Dropdown is present on Page");
		}	
		else 
		{
			System.out.println(homePage_PageObject.dropdown_Region.getText()+" " + "Region Dropdown is NOT present on Page");
		}		
		
		System.out.println("==================================================================================");
		System.out.println("==================================================================================");
		
		
		return null;				
		
	}	
}	
