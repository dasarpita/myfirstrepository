package com.IX.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage_PageObject {
	
	
	@FindBy(id="email")
	WebElement txt_EmailAddress;

	@FindBy(xpath="html/body/div[1]/header/div[1]/a/i")
	WebElement btn_IX;
	
	@FindBy(xpath="html/body/div[1]/div[3]/a/img[1]")
	WebElement btn_ContactUs;
	
	@FindBy(xpath="html/body/div[1]/div[2]")
	WebElement btn_Nav;
	
	@FindBy(xpath=".//*[@id='cust-btn']")
	WebElement btn_WatchOurReel;
	
	@FindBy(xpath=".//*[@id='block-views-carsouel-block-3']/div/section/div/div/div/div/h2/button[2]")
	WebElement btn_LatestWork;
	
	@FindBy(xpath=".//a[contains(@href, '/ideas/icrossing-named-one-ad-ages-agencies-watch')]")
	WebElement link_ToWatch;
	
	@FindBy(xpath="html/body/div[1]/div[1]/div/div/p/a")
	WebElement link_AgenciesToWatch;
	
	@FindBy(xpath=".//*[@id='block-views-featured-work-block']/h2")
	WebElement section_FeaturedWork;
	
	@FindBy(xpath=".//*[@id='demos']/div/div/div/div[2]/div[1]")
	WebElement link_Previous;
	
	@FindBy(xpath=".//*[@id='demos']/div/div/div/div[2]/div[2]")
	WebElement link_Next;	
	
	@FindBy(xpath=".//*[@id='demos']/div/div/div/div[1]/div/div[6]/div/div")
	WebElement tile_1;
	
	@FindBy(css=".media-body>h2")
	WebElement tile_txt;
		
	@FindBy(xpath=".//*[@id='demos']/div/div/div/div[3]/div[1]")
	WebElement Radiobtn_1;
	
	@FindBy(xpath=".//*[@id='demos']/div/div/div/div[3]/div[2]")
	WebElement Radiobtn_2;
	
	@FindBy(xpath=".//*[@id='demos']/div/div/div/div[3]/div[3]")
	WebElement Radiobtn_3;
		
	@FindBy(xpath=".//*[@id='demos']/a")
	WebElement btn_ViewMoreWork;
	
	@FindBy(xpath=".//*[@id='appFooter']/section[1]")
	WebElement section_Footer;
	
	@FindBy(xpath=".//*[@id='appFooter']/section[2]/div/h4")
	WebElement section_FollowUs;
	
	@FindBy(xpath=".//*[@id='appFooter']/section[2]/div/div[1]/a[1]/img")
	WebElement btn_Linkedin;
	
	@FindBy(xpath=".//*[@id='appFooter']/section[2]/div/div[1]/a[2]/img")
	WebElement btn_Facebook;
	
	@FindBy(xpath=".//*[@id='appFooter']/section[2]/div/div[1]/a[3]/img")
	WebElement btn_Twitter;
	
	@FindBy(xpath=".//*[@id='appFooter']/section[2]/div/div[1]/a[4]/img")
	WebElement btn_Instagram;	
	
	@FindBy(xpath=".//*[@id='appFooter']/section[2]/div/div[2]/div[2]/div/select")
	WebElement dropdown_Region;	
	
	@FindBy(xpath=".//*[@id='appFooter']/section[2]/div/p/a/img")
	WebElement btn_FooterIX;	
	
	@FindBy(xpath=".//*[@id='appNav']/div[1]/ul[1]/li[1]/a")
	WebElement link_About;
	
	@FindBy(xpath=".//*[@id='appNav']/div[1]/ul[1]/li[2]/a")
	WebElement link_Work;
	
	@FindBy(xpath=".//*[@id='appNav']/div[1]/ul[1]/li[3]/a")
	WebElement link_services;
	
	@FindBy(xpath=".//*[@id='appNav']/div[1]/ul[1]/li[4]/a")
	WebElement link_ideas;
	
	
		
}
