package com.IX.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class Page_LoginObject {

	public EventFiringWebDriver driver=null;
	
	 
	public Page_LoginObject(EventFiringWebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	/* Login Page : User TextBox */
	@FindBy(id="login") 
	public WebElement Txtbox_User_Email;
	   
	/* Login Page : User Password TextBox */
	@FindBy(id="password")
	public WebElement Txtbox_User_Password;
	   
	/* Login Page : SignIn button xpath */
	@FindBy(id="submit")
	public WebElement Btn_SignIn;
	  
	/* Log off button on a user logged-in screen  */
	@FindBy(css=".btn.btn-inverse.dropdown-toggle")
	public WebElement Dropdown_Toggle;
	
	@FindBy(linkText = "Log Out")
	public WebElement Btn_LogOut;
	
	@FindBy(xpath="//*[@class='pull_nav_toggle']")
	public WebElement NewFlow_logout;
	
	@FindBy (xpath= "//a[@href='/auth/logout']")
	public WebElement Link_Logout;

}
