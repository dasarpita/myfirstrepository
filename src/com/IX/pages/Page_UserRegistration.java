package com.IX.pages;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;





import com.IX.dataobject.UserRegistrationFieldNames;
import com.IX.framework.Driver;
import com.IX.utilities.FrequenceSeleniumConfiguration;
import com.IX.utilities.GenericServiceController;

public class Page_UserRegistration extends Driver {
	
	Page_UserRegistrationObject page_UserRegistrationObject;
	Page_LoginObject page_LoginObject;
	Page_CommonObjects page_CommonObjects;
	GenericServiceController genericServiceController;
	
	public Page_UserRegistration (EventFiringWebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		page_LoginObject = new Page_LoginObject(driver);
		page_UserRegistrationObject = PageFactory.initElements(driver, Page_UserRegistrationObject.class);
		page_CommonObjects = PageFactory.initElements(driver, Page_CommonObjects.class);
		genericServiceController = new GenericServiceController(driver);
	}
	
	
	public String createNewUser(UserRegistrationFieldNames userRegistrationFieldNames,String userEmailAddress){
		String selectedUserRole= userRegistrationFieldNames.getUserRole();
		page_CommonObjects.drop_toggle.click();
		page_CommonObjects.btn_register.click();
		page_UserRegistrationObject.txt_EmailAddress.sendKeys(userEmailAddress);
		page_UserRegistrationObject.txt_Password.sendKeys(FrequenceSeleniumConfiguration.getConfigConstants().getTestAdminPassword());
		page_UserRegistrationObject.txt_ConfirmPassword.sendKeys(FrequenceSeleniumConfiguration.getConfigConstants().getTestAdminPassword());
		page_UserRegistrationObject.txt_FirstName.sendKeys(userRegistrationFieldNames.getFirstName());
		page_UserRegistrationObject.txt_LastName.sendKeys(userRegistrationFieldNames.getLastName());
		
		if (selectedUserRole.equals("Admin") || selectedUserRole.equals("Ops") || selectedUserRole.equals("Creative")){
			
			genericServiceController.selectDataFromDropDown(driver, page_UserRegistrationObject.drop_SelectRole, "selectElementByText", selectedUserRole);
						
		}else if(selectedUserRole.equals("Advertiser")){
			
			genericServiceController.selectDataFromDropDown(driver, page_UserRegistrationObject.drop_SelectRole, "selectElementByText", selectedUserRole);
			page_UserRegistrationObject.drop_AdvertiserOrg.click();
			page_UserRegistrationObject.txt_DropFocused.sendKeys(userRegistrationFieldNames.getAdvertiserOrg());
			page_UserRegistrationObject.select_ValueFromDropdownForRegistration(driver, userRegistrationFieldNames.getAdvertiserOrg()).click();
			
			selectRequiredCheckBox(userRegistrationFieldNames);	
									
		}else if (selectedUserRole.equals("Sales")){
			
			genericServiceController.selectDataFromDropDown(driver, page_UserRegistrationObject.drop_SelectRole, "selectElementByText", selectedUserRole);
			page_UserRegistrationObject.drop_Partner.click();
			page_UserRegistrationObject.txt_DropFocused.sendKeys(userRegistrationFieldNames.getPartner());
			page_UserRegistrationObject.select_ValueFromDropdownForRegistration(driver, userRegistrationFieldNames.getPartner()).click();
			page_UserRegistrationObject.txt_Address1.sendKeys(userRegistrationFieldNames.getAddress());
			page_UserRegistrationObject.txt_Address2.sendKeys(userRegistrationFieldNames.getAddressLine2());
			page_UserRegistrationObject.txt_City.sendKeys(userRegistrationFieldNames.getCity());
			page_UserRegistrationObject.txt_State.sendKeys(userRegistrationFieldNames.getStateProvince());
			page_UserRegistrationObject.txt_ZipCode.sendKeys(userRegistrationFieldNames.getZipPostalCode());
			page_UserRegistrationObject.txt_PhoneNumber.sendKeys(userRegistrationFieldNames.getPhoneNumber());
			
			
		}else {
			System.out.println("Please check values for user role in test data file");
		}
		uncheckAllCheckbox();
		selectRequiredCheckBox(userRegistrationFieldNames);
		page_UserRegistrationObject.btn_Register.click();
		
		genericServiceController.waitForElementToBeVisibleNew(driver, page_UserRegistrationObject.msg_Success);
		
		String successMsg = page_UserRegistrationObject.msg_Success.getText().replace("×", "");
		String newsuccessMsg = successMsg.trim();
					
		return newsuccessMsg;
		
		
		
	}
	public void selectRequiredCheckBox(UserRegistrationFieldNames userRegistrationFieldNames){
		if (userRegistrationFieldNames.getIsSuper().toLowerCase().equals("yes")){
			
			page_UserRegistrationObject.check_IsGroupSuper.click();
			
		}else{
			// Do nothing
		}
		if ( userRegistrationFieldNames.getCanViewOldPlanner().toLowerCase().equals("yes")){
			page_UserRegistrationObject.check_IsOldPlanner.click();
		}else{
			// Do nothing
		}
		if ( userRegistrationFieldNames.getCanViewPlacementsInReports().toLowerCase().equals("yes")){
			page_UserRegistrationObject.check_IsPlacementsInReport.click();
		}else{
			// Do nothing
		}
		
		if ( userRegistrationFieldNames.getCanViewScreenshotsInReports().toLowerCase().equals("yes") ){
			page_UserRegistrationObject.check_IsScreenshotsInReport.click();
		}else{
			// Do nothing
		}
		if ( userRegistrationFieldNames.getCanViewEngagementsInReports().toLowerCase().equals("yes")){
			page_UserRegistrationObject.check_IsEngagementsInReport.click();
		}else{
			// Do nothing
		}
				
		if(userRegistrationFieldNames.getAutomaticallySendWelcomeEmail().toLowerCase() .equals("yes")){
			
			page_UserRegistrationObject.redio_SendEmail.click();
			
		}else{
			page_UserRegistrationObject.redio_CopyPasteCredentials.click();
		}
	}
	
	
	public void uncheckAllCheckbox(){
		if (page_UserRegistrationObject.check_IsOldPlanner.isSelected()){
		
			page_UserRegistrationObject.check_IsOldPlanner.click();
		}else{
			//System.out.println("do nothing");
		}
		if (page_UserRegistrationObject.check_IsPlacementsInReport.isSelected()){
			
			page_UserRegistrationObject.check_IsPlacementsInReport.click();
		}else{
			//System.out.println("do nothing");
		}
		if (page_UserRegistrationObject.check_IsScreenshotsInReport.isSelected()){
			
			page_UserRegistrationObject.check_IsScreenshotsInReport.click();
		}else{
			//System.out.println("do nothing");
		}
		if (page_UserRegistrationObject.check_IsEngagementsInReport.isSelected()){
			
			page_UserRegistrationObject.check_IsEngagementsInReport.click();
		}else{
			//System.out.println("do nothing");
		}
		
		
	}
	

}
