package com.IX.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.IX.dataobject.UserRegistrationDataObject;
import com.IX.dataobject.UserRegistrationFieldNames;
import com.IX.pages.Page_RoleBaseLogins;
import com.IX.pages.Page_UserRegistration;
import com.IX.utilities.GenericServiceController;
//import com.frequence.pages.Page_CreativeRequest;

public class UserRegistrationTest extends Page_RoleBaseLogins {
	
	GenericServiceController genericServiceController = new GenericServiceController();
	
	@Test(groups={"stage"},dataProvider = "getUserRegistrationDetailsDataInObjects", dataProviderClass = UserRegistrationDataObject.class)
	
	public void registerNewUser(UserRegistrationFieldNames userRegistrationFieldNames) {
		String userEmailAddress = userRegistrationFieldNames.getEmailAddress()+ genericServiceController.getCurrentDate()+"@frequence.com";
					
		this.testAdminUserLogin();
		Page_UserRegistration page_UserRegistration = new Page_UserRegistration(driver);
				
		String userCreated = page_UserRegistration.createNewUser(userRegistrationFieldNames,userEmailAddress);
		
		Assert.assertEquals(userCreated,"New user successfully created!");
		this.logoutNewFlow();
	}
	
	

}
