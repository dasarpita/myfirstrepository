package com.IX.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.IX.dataobject.HomePage_DataObject;
import com.IX.dataobject.HomePage_FieldNames;
import com.IX.framework.Driver;
import com.IX.pages.HomePage_Page;
import com.IX.utilities.GenericServiceController;

public class HomePageTest extends Driver{
	
	//common methods class -> 
	GenericServiceController genericServiceController = new GenericServiceController();	
	//date comes from respective data object class -> 
	@Test(groups={"stage"},dataProvider = "getHomePageDetailsDataInObjects", dataProviderClass = HomePage_DataObject.class)
	
	public void verifyHomePage (HomePage_FieldNames homePage_FieldNames) throws InterruptedException 
	{
		//get values from field names class - 
		String userEmailAddress = homePage_FieldNames.getEmailAddress()+ genericServiceController.getCurrentDate()+"@frequence.com";
		//create object for home page - 
		HomePage_Page homePage_Page = new HomePage_Page(driver);
		//access home page method & pass values - 
		String pageVerified = homePage_Page.verifyPage(homePage_FieldNames,userEmailAddress);
		Assert.assertEquals(pageVerified,"New user successfully created!");			
	}
}


