package com.IX.listeners;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

import com.IX.framework.Driver;
import com.IX.utilities.GenericServiceController;
import com.IX.utilities.SendStatusEmail;
import com.relevantcodes.extentreports.LogStatus;

public class ExtentReporterNG extends Driver implements IReporter{
	
	
	//private ExtentReports extent;
	SendStatusEmail sendStatusEmail = new SendStatusEmail();
	GenericServiceController genericservicecontroller = new GenericServiceController();  	
    @Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
        
        for (ISuite suite : suites) {
            Map<String, ISuiteResult> result = suite.getResults();
 
            for (ISuiteResult r : result.values()) {
                ITestContext context = r.getTestContext();
 
                buildTestNodes(context.getPassedTests(), LogStatus.PASS);
                buildTestNodes(context.getFailedTests(), LogStatus.FAIL);
                buildTestNodes(context.getSkippedTests(), LogStatus.SKIP);
            }
        }
        
        //extent.flush();
       // extent.close();
       sendStatusEmail.sendMail(reportpath);
    }

   private void buildTestNodes(IResultMap tests, LogStatus status) {
        //ExtentTest test = null;
 
        if (tests.size() > 0) {
            for (ITestResult result : tests.getAllResults()) {
                //test = extent.startTest(result.getMethod().getMethodName());
 
               // reportTest.setStartedTime(getTime(result.getStartMillis()));
              //  reportTest.setEndedTime(getTime(result.getEndMillis()));

               /* if (result.getThrowable() != null) {
                    //test.log(status, result.getThrowable());
                }
                else {
                    test.log(status, "Test " + status.toString().toLowerCase() + "ed");
                } */
            }
        }
    }
    
	/*
	  String reportFileName;
	  String folderPath;
	  static Date dt = new Date();
	  static SimpleDateFormat folderNameSf = new SimpleDateFormat("MMM_dd_yyyy");
	  static SimpleDateFormat resultFileNameSf = new SimpleDateFormat("hh_mm_ss");
	  
	  @Override
	  public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
	    reportFileName = "EngageAutomationTestReports_" + resultFileNameSf.format(dt)+".html";
	    folderPath = folderNameSf.format(dt);
	    extent =
	        new ExtentReports(EngageSeleniumConfiguration.getConfigConstants().getExcelOutputReportsPath() 
	            + folderPath + File.separator + reportFileName,
	            true, DisplayOrder.NEWEST_FIRST);
	    extent.loadConfig(new File(EngageSeleniumConfiguration.getConfigConstants()
	        .getExtentReportsConfigPath()));
	    for (ISuite suite : suites) {
	      Map<String, ISuiteResult> result = suite.getResults();
	      for (ISuiteResult suiteResult : result.values()) {
	        ITestContext context = suiteResult.getTestContext();
	        // System.out.println("Suite Name "+context.getName()+"\n"+" Name "+context.getSuite().getName());
	        buildTestNodes(context.getPassedTests(), LogStatus.PASS);
	        buildTestNodes(context.getFailedTests(), LogStatus.FAIL);
	        buildTestNodes(context.getSkippedTests(), LogStatus.SKIP);
	      }
	    }
	    extent.flush();
	    extent.close();
	  }
	
    private void buildTestNodes(IResultMap tests, LogStatus status) {
        ExtentTest test;
        if (tests.size() > 0) {
          for (ITestResult result : tests.getAllResults()) {
            test =
                extent
                    .startTest(result.getMethod().getMethodName(), "Category")
                    .assignAuthor("Cybage QA")
                    .assignCategory(
                        "Regresion on " + this.strBrowser + genericservicecontroller.dateFormat());
            test.getTest().setStartedTime(genericservicecontroller.getTime(result.getStartMillis()));
            test.getTest().setEndedTime(genericservicecontroller.getTime(result.getEndMillis()));
            for (String strGroup : result.getMethod().getGroups())
              test.assignCategory(strGroup);
            String strMessage = "Test " + status.toString().toLowerCase() + "ed";
            if (result.getThrowable() != null)
              strMessage = result.getThrowable().getMessage();
            test.log(status, strMessage);
            extent.endTest(test);
          }
        }
      }
 
 
*/
   
   private Date getTime(long millis) {
       Calendar calendar = Calendar.getInstance();
       calendar.setTimeInMillis(millis);
       return calendar.getTime();        
   }
}
