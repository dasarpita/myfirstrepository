package com.IX.listeners;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.testng.Reporter;

import com.IX.framework.Driver;
import com.relevantcodes.extentreports.LogStatus;

public class TestEventHandler extends Driver implements WebDriverEventListener  {
	public int stepNo=0;
	
	private WebDriver wbdriver;
	
	public TestEventHandler(WebDriver wbdriver){ 
		this.wbdriver = wbdriver; 
	} 

	public void afterChangeValueOf(WebElement arg0, WebDriver arg1) {
		stepNo=stepNo+1;
		Reporter.log(stepNo+ "-		inside method afterChangeValueOf on " + arg0.toString());
		log.info(stepNo+ "-		inside method afterChangeValueOf on " + arg0.toString());
		//reportTest.log(LogStatus.PASS, "After Change Value Of" + arg0.toString());
	}
 
	public void afterClickOn(WebElement arg0, WebDriver arg1) {
		stepNo=stepNo+1;
		Reporter.log(stepNo + "-		Clicked Sucessfully at" + arg0.toString());
		log.info(stepNo + "-		Clicked Sucessfully at" + arg0.toString());
		//reportTest.log(LogStatus.PASS, "Clicked Sucessfully at" + arg0.toString());
	}
 
	public void afterFindBy(By arg0, WebElement arg1, WebDriver arg2) {
	}
 
	public void afterNavigateBack(WebDriver arg0) {
		stepNo=stepNo+1;
		Reporter.log(stepNo + "-		Navigated back to " + arg0.getCurrentUrl());
		log.info(stepNo + "-		Navigated back to " + arg0.getCurrentUrl());
		//reportTest.log(LogStatus.PASS, "Navigated back to " + arg0.getCurrentUrl());
	}
 
	public void afterNavigateForward(WebDriver arg0) {
		stepNo=stepNo+1;
		Reporter.log(stepNo + "-		Navigated Forward to " + arg0.getCurrentUrl());
		log.info(stepNo + "-		Navigated Forward to " + arg0.getCurrentUrl());
		//reportTest.log(LogStatus.PASS, "Navigated Forward to " + arg0.getCurrentUrl());
	}
 
	public void afterNavigateTo(String arg0, WebDriver arg1) {
		stepNo=stepNo+1;
		Reporter.log(stepNo + "-		Navigated to " + arg0);
		log.info(stepNo + "-		Navigated to " + arg0);
		//reportTest.log(LogStatus.PASS, "Navigated to " + arg0);
	} 
 
	public void afterScript(String arg0, WebDriver arg1) {
	}
 
	public void beforeChangeValueOf(WebElement arg0, WebDriver arg1) {
	}
																		
	public void beforeClickOn(WebElement arg0, WebDriver arg1) {
 	}
 
	public void beforeFindBy(By arg0, WebElement arg1, WebDriver arg2) {
 	}
 
	public void beforeNavigateBack(WebDriver arg0) {
 	}
 
	public void beforeNavigateForward(WebDriver arg0) {
 	}
 
	public void beforeNavigateTo(String arg0, WebDriver arg1) {
	}
 
	public void beforeScript(String arg0, WebDriver arg1) {
	}
 
	@Override
	public void afterNavigateRefresh(WebDriver arg0) {
	}

	@Override
	public void beforeNavigateRefresh(WebDriver arg0) {
	}

	@Override
	public void onException(Throwable arg0, WebDriver arg1) {
		// TODO Auto-generated method stub
		
	}

	/*public void onException(Throwable arg0, WebDriver arg1) {
		stepNo=stepNo+1;
		Reporter.log(stepNo + "-		Exception occured at " + arg0.getMessage());
		//("<b> <font color='red'>Hello world</font></b>",
		System.out.println(stepNo + "- 	Exception occured at " + arg0.getClass());
		log.info(stepNo + "-		Exception occured at " + arg0.getMessage());
		reportTest.log(LogStatus.FAIL, "Exception occured at " + arg0.getMessage());
	}*/
	
	
}

