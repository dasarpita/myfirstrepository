package com.IX.listeners;

import org.testng.IClass;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import com.IX.framework.AssertManager;
import com.IX.framework.FrameworkBase;

public class CustomTestListenerAdapter extends TestListenerAdapter {

	FrameworkBase frameworkBase;
	public CustomTestListenerAdapter() {
		frameworkBase = new FrameworkBase();
	}
	@Override
	public void onTestStart(ITestResult tr) {
		frameworkBase = new FrameworkBase();
		log("Test Started....");
	}

	@Override
	public void onTestSuccess(ITestResult tr) {
	    String verificationErrorString = AssertManager.verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	     // fail(verificationErrorString);    
	    	log("Test '" + tr.getName() + "' Failed");
			log(tr.getTestClass());
			//FrameworkBase.reportTest.log(LogStatus.FAIL, verificationErrorString);
	    }
	    else {
		log("Test '" + tr.getName() + "' PASSED");
		log(tr.getTestClass());
		//FrameworkBase.reportTest.log(LogStatus.PASS, "Success");
	    }
	}
	
	
	@Override
	public void onTestFailure(ITestResult tr) {
		try {
		log(tr.getThrowable());
		log("Test '" + tr.getName() + "' FAILED"); //tr.getName gives test method Name
		log("Priority of this method is " + tr.getName());
		if (tr.getThrowable().getMessage() != null){
			//FrameworkBase.reportTest.log(LogStatus.FAIL, tr.getThrowable().getMessage());
		}
		//frameworkBase.takeScreenshots(tr.getName()); // for taking and showing screenshot on reports)
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onTestSkipped(ITestResult tr) {
		log("Test '" + tr.getName() + "' SKIPPED");
		System.out.println(".....");
	}

	private void log(Throwable throwable) {
		System.out.println(throwable);
	}
	
	private void log(String methodName) {
		System.out.println(methodName);
	}

	private void log(IClass testClass) {
		System.out.println(testClass);
	}
}