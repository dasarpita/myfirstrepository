package com.IX.dataobject;

public class HomePage_FieldNames {
	
	public static final String EmailAddressConstant ="Email Address";
		
	private String emailAddress;
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
}
