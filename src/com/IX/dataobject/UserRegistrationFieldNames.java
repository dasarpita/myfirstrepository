package com.IX.dataobject;

public class UserRegistrationFieldNames {
	
	public static final String EmailAddressConstant ="Email Address";
	public static final String FirstNameConstant ="First Name";
	public static final String LastNameConstant ="Last Name";
	public static final String UserRoleConstant ="User Role";
	public static final String AdvertiserOrgConstant ="Advertiser Org";
	public static final String CanViewOldPlannerConstant ="Can view old planner?";
	public static final String CanViewPlacementsInReportsConstant ="Can view placements in reports?";
	public static final String CanViewScreenshotsInReportsConstant ="Can view screenshots in reports?";
	public static final String CanViewEngagementsInReportsConstant ="Can view engagements in reports?";
	public static final String CopyPasteCredentialsConstant ="Copy paste credentials";
	public static final String AutomaticallySendWelcomeEmailConstant ="Automatically send welcome email?";
	public static final String AddressConstant ="Address";
	public static final String AddressLine2Constant ="Address (Line 2)";
	public static final String CityConstant ="City";
	public static final String StateProvinceConstant ="State / Province";
	public static final String ZipPostalCodeConstant ="Zip / Postal Code";
	public static final String PhoneNumberConstant ="Phone Number";
	public static final String PartnerConstant ="Partner";
	public static final String IsSuperConstant ="is super?";
	
	private String emailAddress;
	private String firstName;
	private String lastName;
	private String userRole;
	private String advertiserOrg;
	private String canViewOldPlanner;
	private String canViewPlacementsInReports;
	private String canViewScreenshotsInReports;
	private String canViewEngagementsInReports;
	private String copyPasteCredentials;
	private String automaticallySendWelcomeEmail;
	private String address;
	private String addressLine2;
	private String city;
	private String stateProvince;
	private String zipPostalCode;
	private String phoneNumber;
	private String partner;
	private String isSuper;
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getAdvertiserOrg() {
		return advertiserOrg;
	}
	public void setAdvertiserOrg(String advertiserOrg) {
		this.advertiserOrg = advertiserOrg;
	}
	public String getCanViewOldPlanner() {
		return canViewOldPlanner;
	}
	public void setCanViewOldPlanner(String canViewOldPlanner) {
		this.canViewOldPlanner = canViewOldPlanner;
	}
	public String getCanViewEngagementsInReports() {
		return canViewEngagementsInReports;
	}
	public void setCanViewEngagementsInReports(
			String canViewEngagementsInReports) {
		this.canViewEngagementsInReports = canViewEngagementsInReports;
	}
	public String getCanViewPlacementsInReports() {
		return canViewPlacementsInReports;
	}
	public void setCanViewPlacementsInReports(String canViewPlacementsInReports) {
		this.canViewPlacementsInReports = canViewPlacementsInReports;
	}
	public String getCopyPasteCredentials() {
		return copyPasteCredentials;
	}
	public void setCopyPasteCredentials(String copyPasteCredentials) {
		this.copyPasteCredentials = copyPasteCredentials;
	}
	public String getAutomaticallySendWelcomeEmail() {
		return automaticallySendWelcomeEmail;
	}
	public void setAutomaticallySendWelcomeEmail(
			String automaticallySendWelcomeEmail) {
		this.automaticallySendWelcomeEmail = automaticallySendWelcomeEmail;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getStateProvince() {
		return stateProvince;
	}
	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipPostalCode() {
		return zipPostalCode;
	}
	public void setZipPostalCode(String zipPostalCode) {
		this.zipPostalCode = zipPostalCode;
	}
	public String getIsSuper() {
		return isSuper;
	}
	public void setIsSuper(String isSuper) {
		this.isSuper = isSuper;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	public String getCanViewScreenshotsInReports() {
		return canViewScreenshotsInReports;
	}
	public void setCanViewScreenshotsInReports(
			String canViewScreenshotsInReports) {
		this.canViewScreenshotsInReports = canViewScreenshotsInReports;
	}
	
	
	

}
