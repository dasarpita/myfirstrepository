package com.IX.dataobject;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import org.testng.annotations.DataProvider;

import com.IX.utilities.FileHandlingOperation;
import com.IX.utilities.GenericServiceController;

public class UserRegistrationDataObject {

	public final static String path = System.getProperty("user.dir");

	public static Object[][] getUserRegistrationDetailsDataInObjects(
			String strTabName) {
		String strFilePath = path + "/testdata/TestDataNew.xlsx";
		System.out.println("Test data file " + strFilePath);
		Map<String, ArrayList<String>> dataTreeMap = FileHandlingOperation
				.readFromExcelFile(strFilePath, strTabName);
		Set<String> entityKeyList = dataTreeMap.keySet();
		Object[][] dataEntityObject = new Object[entityKeyList.size() - 1][1];
		int keyIndex = 0;
		for (int key = 1; key < entityKeyList.size(); key++) {

			Map<String, String> entityFieldsDetailMap = GenericServiceController
					.getEntityDetailsFromMap(Integer.toString(key), dataTreeMap);

			UserRegistrationFieldNames userRegistrationFieldNames = new UserRegistrationFieldNames();

			userRegistrationFieldNames.setEmailAddress(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.EmailAddressConstant));
			
			userRegistrationFieldNames.setFirstName(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.FirstNameConstant));
			userRegistrationFieldNames.setLastName(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.LastNameConstant));
			
			userRegistrationFieldNames.setUserRole(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.UserRoleConstant));
			userRegistrationFieldNames.setAdvertiserOrg(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.AdvertiserOrgConstant));
			
			userRegistrationFieldNames
					.setCanViewOldPlanner(entityFieldsDetailMap
							.get(UserRegistrationFieldNames.CanViewOldPlannerConstant));
			
			userRegistrationFieldNames
					.setCanViewPlacementsInReports(entityFieldsDetailMap
							.get(UserRegistrationFieldNames.CanViewPlacementsInReportsConstant));
			userRegistrationFieldNames
					.setCanViewScreenshotsInReports(entityFieldsDetailMap
							.get(UserRegistrationFieldNames.CanViewScreenshotsInReportsConstant));
			userRegistrationFieldNames
					.setCanViewEngagementsInReports(entityFieldsDetailMap
							.get(UserRegistrationFieldNames.CanViewEngagementsInReportsConstant));
			userRegistrationFieldNames
					.setCopyPasteCredentials(entityFieldsDetailMap
							.get(UserRegistrationFieldNames.CopyPasteCredentialsConstant));
			userRegistrationFieldNames
					.setAutomaticallySendWelcomeEmail(entityFieldsDetailMap
							.get(UserRegistrationFieldNames.AutomaticallySendWelcomeEmailConstant));
			userRegistrationFieldNames.setAddress(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.AddressConstant));
			userRegistrationFieldNames.setAddressLine2(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.AddressLine2Constant));
			
			userRegistrationFieldNames.setCity(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.CityConstant));
			
			userRegistrationFieldNames.setStateProvince(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.StateProvinceConstant));
			
			userRegistrationFieldNames.setZipPostalCode(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.ZipPostalCodeConstant));
			
			userRegistrationFieldNames.setPhoneNumber(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.PhoneNumberConstant));
			
			userRegistrationFieldNames.setPartner(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.PartnerConstant));
			
			userRegistrationFieldNames.setIsSuper(entityFieldsDetailMap
					.get(UserRegistrationFieldNames.IsSuperConstant));
			dataEntityObject[keyIndex][0] = userRegistrationFieldNames;
			keyIndex++;
			
		}

		return dataEntityObject;

	}
	
	
	/**
	   * Data provider
	   * 
	   * @return
	   */
	@DataProvider(name = "getUserRegistrationDetailsDataInObjects") 
	public static Object[][] getUserRegistrationDetails() {
	    String strTabName = "UserRegistration";
	    Object[][] userRegistrationDetailsObjects = getUserRegistrationDetailsDataInObjects(strTabName);
	    
	  //  System.out.println("Ajay" + userRegistrationDetailsObjects[0][0]);
	    return userRegistrationDetailsObjects;
	}
	

}

