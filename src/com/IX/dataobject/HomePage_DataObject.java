package com.IX.dataobject;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import org.testng.annotations.DataProvider;
import com.IX.utilities.FileHandlingOperation;
import com.IX.utilities.GenericServiceController;

public class HomePage_DataObject {

	public final static String path = System.getProperty("user.dir");

	public static Object[][] getHomePageDetailsDataInObjects(String strTabName) 
	
	{
		String strFilePath = path + "/testdata/TestDataNew.xlsx";
		System.out.println("Test data file " + strFilePath);
		
		Map<String, ArrayList<String>> dataTreeMap = FileHandlingOperation.readFromExcelFile(strFilePath, strTabName);
		Set<String> entityKeyList = dataTreeMap.keySet();
		
		Object[][] dataEntityObject = new Object[entityKeyList.size() - 1][1];
		
		int keyIndex = 0;
		for (int key = 1; key < entityKeyList.size(); key++) {

			Map<String, String> entityFieldsDetailMap = GenericServiceController.getEntityDetailsFromMap(Integer.toString(key), dataTreeMap);

			HomePage_FieldNames homePage_FieldNames = new HomePage_FieldNames();
			homePage_FieldNames.setEmailAddress(entityFieldsDetailMap.get(HomePage_FieldNames.EmailAddressConstant));
			
			dataEntityObject[keyIndex][0] = homePage_FieldNames;
			keyIndex++;
			
		}

		return dataEntityObject;

	}
	
	
	/**
	   * Data provider
	   * 
	   * @return
	   */
	@DataProvider(name = "getHomePageDetailsDataInObjects") 
	public static Object[][] getHomePageDetails() {
	    String strTabName = "UserRegistration";
	    Object[][] userHomePageDetailsObjects = getHomePageDetailsDataInObjects(strTabName);
	    return userHomePageDetailsObjects;
	}
	

}

