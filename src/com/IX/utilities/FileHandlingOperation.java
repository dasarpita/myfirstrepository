package com.IX.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FileHandlingOperation {

	/**
	 * This method perform read data from Excel file operation from given
	 * specified path location and put data into tree map into object form and
	 * will provide this data at the test script.
	 * 
	 * @param filePath
	 *            : Excel file path location from server hard drive.
	 * 
	 * @return It returns treeMap consist of multiple data objects with unique
	 *         key.
	 */
	public static Map<String, ArrayList<String>> readFromExcelFile(
			String filePath, String tabName) {
		Map<String, ArrayList<String>> dataTreeMap = new TreeMap<String, ArrayList<String>>();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("d-MMMM-yyyy");
		try {
			FileInputStream file = new FileInputStream(new File(filePath));
			XSSFWorkbook workbook = null;
			// Create Workbook instance holding reference to .xlsx file
			try {
				workbook = new XSSFWorkbook(file);
			} catch (Exception e) {
				System.out.println(e);
			}
			// Get desired name of tab in Excel sheet from the workbook
			XSSFSheet sheet = workbook.getSheet(tabName);

			// Formula evaluator - will evaluate formula in each cell.
			FormulaEvaluator evaluator = workbook.getCreationHelper()
					.createFormulaEvaluator();

			// Iterate through each rows one by one
			Iterator<Row> rowIterator = null;
			try {
				rowIterator = sheet.iterator();
			} catch (Exception e) {
				System.out.println(e);
			}
			int keyIndex = 0;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				ArrayList<String> proposalFieldsList = new ArrayList<String>();
				int campaignFieldsIndex = 0;
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					// Check the cell type and format accordingly
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_FORMULA:
						evaluator.evaluateFormulaCell(cell);
					case Cell.CELL_TYPE_NUMERIC:
						if (HSSFDateUtil.isCellDateFormatted(cell)) {
							proposalFieldsList.add(dateFormatter.format(cell
									.getDateCellValue()));
							//System.out.println(dateFormatter.format(cell
								//	.getDateCellValue()));
						} else {
							DataFormatter formatter = new DataFormatter(Locale.US);
							System.out.println(formatter.formatCellValue(cell));
							//long cellValue = (long) (cell.getNumericCellValue());
							proposalFieldsList.add(campaignFieldsIndex,
									formatter.formatCellValue(cell));
						}
						break;
					case Cell.CELL_TYPE_STRING:
						proposalFieldsList.add(campaignFieldsIndex, cell
								.getStringCellValue().trim());
						//System.out.println(cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						String cellValue = String.valueOf(cell
								.getBooleanCellValue());
						proposalFieldsList.add(campaignFieldsIndex, cellValue);
						break;
					case Cell.CELL_TYPE_BLANK:
						cell.setCellValue("");
						proposalFieldsList.add(campaignFieldsIndex,
								cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_ERROR:
						proposalFieldsList.add(campaignFieldsIndex,
								cell.getStringCellValue());
						break;
					default:
						proposalFieldsList.add(campaignFieldsIndex,
								cell.getStringCellValue());
						break;
					}
					campaignFieldsIndex++;
				}
				dataTreeMap.put(Integer.toString(keyIndex), proposalFieldsList);
				keyIndex++;
			}
			file.close();
		} catch (FileNotFoundException e) {
			// Log.error(e.getMessage());
		} catch (IOException e) {
			// Log.error(e.getMessage());
		}
		return dataTreeMap;
	}
}
