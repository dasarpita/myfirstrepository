package com.IX.utilities;

public class FrequenceConfigurationConstants {

	/* Project Env URL */
	public String getLoginURL() {
	    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.app.test.url");
	}
	
	/* User Roles */
	public String getTestAdminUserName() {
	    return FrequenceSeleniumConfiguration
	        .getProperty("frequence.selenium.admin.user.name");
	}

	public String getTestAdminPassword() {
		return FrequenceSeleniumConfiguration
	        .getProperty("frequence.selenium.admin.user.pwd");
	}
	
	public String getSalesUserName() {
	    return FrequenceSeleniumConfiguration
	        .getProperty("frequence.selenium.sales.user.name");
	}

	public String getSalesPassword() {
		return FrequenceSeleniumConfiguration
	        .getProperty("frequence.selenium.sales.user.pwd");
	}
	
	public String getOpsUserName() {
	    return FrequenceSeleniumConfiguration
	        .getProperty("frequence.selenium.ops.user.name");
	}

	public String getOpsPassword() {
		return FrequenceSeleniumConfiguration
	        .getProperty("frequence.selenium.ops.user.pwd");
	}
	
	public String getAdvertiserUserName() {
	    return FrequenceSeleniumConfiguration
	        .getProperty("frequence.selenium.advertiser.user.name");
	}

	public String getAdvertiserPassword() {
		return FrequenceSeleniumConfiguration
	        .getProperty("frequence.selenium.advertiser.user.pwd");
	}
	
	  /**
	   * Project Reports
	   * */
	  public String getExtentReportsPath() {
	    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.extentreports.path");
	  }

	  public String getExtentReportsConfigPath() {
	    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.extentreports.config.path");
	  }

	  public String getExcelOutputReportsPath() {
	    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.exceloutput.reports.path");
	  }
	  
	  /*
	   * Get properties for Email configurations.
	   */
	  public String getMailToAddress() {
	    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.mail.to.address");
	  }

	  public String getMailBccAddress() {
		    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.mail.to.address");
	  }
	  
	  public String getMailFromAddress() {
	    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.mail.from.address");
	  }

	  public String getMailCcAddress() {
	    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.mail.cc.address");
	  }

	  public String getMailHost() {
	    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.mail.mailgun.host");
	  }

	  public String getMailApiKey() {
	    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.mail.mailgun.api.key");
	  }

	  public String getMailSubject() {
	    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.mail.subject");
	  }

	  public String getMailBodyContents() {
		    return FrequenceSeleniumConfiguration.getProperty("frequence.selenium.mail.bodycontents");
	  }
}
