package com.IX.utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.internal.Nullable;

import com.IX.pages.Page_CommonObjects;
import com.google.common.base.Predicate;
import com.sun.org.apache.xpath.internal.functions.Function;

public class GenericServiceController {
	
	JavascriptExecutor jsExecutor;
	String winHandleBefore;
	Page_CommonObjects page_CommonObjects;
	WebDriverWait wait;

	public GenericServiceController() {

	}
	public GenericServiceController(EventFiringWebDriver driver) {
		
		page_CommonObjects = PageFactory.initElements(driver, Page_CommonObjects.class);
		jsExecutor = (JavascriptExecutor)driver;
	}
	

	/**
	 * Entity details map that will return key value pairs from excel sheet with
	 * correct pairs of entity field name with its respective values.
	 * 
	 * @param campaignKeyNumber
	 *            Row number from excel sheet for which we want to retrieve
	 *            record details.
	 * @param dataTreeMap
	 *            dataTreeMap will contains information of all records for
	 *            respective tabs from excel sheet.
	 * @return It will return entity details from given records with its key
	 *         value pair form.
	 */
	public static Map<String, String> getEntityDetailsFromMap(
			String proposalKeyNumber, Map<String, ArrayList<String>> dataTreeMap) {
		Map<String, String> entityFieldsDetailMap = new HashMap<String, String>();
		ArrayList<String> entityFieldNameKeyList = dataTreeMap.get("0");
		ArrayList<String> entityFieldValueList = dataTreeMap
				.get(proposalKeyNumber);
		for (int arrayIndex = 0; arrayIndex < entityFieldNameKeyList.size(); arrayIndex++) {
			entityFieldsDetailMap.put(entityFieldNameKeyList.get(arrayIndex),
					entityFieldValueList.get(arrayIndex));

		}
		return entityFieldsDetailMap;
	}

	/**
	 * This method is to get current date.
	 * 
	 * @return Returns date into string format.
	 */
	public String getCurrentDate() {
		SimpleDateFormat dateFormatter = new SimpleDateFormat(
				"YYYY-MMM-dd-HH-mm-ss-SSS");
		Date currentDate = new Date();
		return dateFormatter.format(currentDate);
	}

	  /**
	   * This method returns current data in format mm/dd/yy
	   * 
	   * @return
	   */
	  public String dateFormat() {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
	    dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
	    return dateFormat.format(new Date());

	  }
	  
	  /**
	   * Get Time in Mill sec
	   * 
	   * @param long
	   * */
	  public Date getTime(long millis) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(millis);
	    return calendar.getTime();
	  }

	/**
	 * This methods gives select options to user
	 * 
	 * @param driver
	 * @param element
	 * @param selectElementBy
	 * @param data
	 */
	public void selectDataFromDropDown(EventFiringWebDriver driver,
			WebElement element, String selectElementBy, String strData) {

		Select selectOptions = new Select(element);
		switch (selectElementBy) {
		case "selectElementByIndex":
			int i = Integer.parseInt(strData);
			selectOptions.selectByIndex(i);
			break;
		case "selectElementByValue":
			selectOptions.selectByValue(strData);
			break;
		case "selectElementByText":
			selectOptions.selectByVisibleText(strData);
			break;
		default:
			System.out.println("Invalid Option");

		}

	}

	/**
	 * This methods gives select options to user
	 * 
	 * @param driver
	 * @param element
	 * @param selectElementBy
	 * @param data
	 */
	public WebElement getSelectedValueFromDropDown(WebElement element) {
		Select selectOptions = new Select(element);
		WebElement ele = selectOptions.getFirstSelectedOption();
		System.out.println(ele.getText());
		return ele;
	}

	/**
	 * Waits till the element is found
	 * 
	 * @param element
	 *            which element to find
	 */
	public void waitForElementToBeClickable(EventFiringWebDriver driver,
			WebElement element) {
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public void waitForElementToBeClickableNew(EventFiringWebDriver driver,
			WebElement element) {
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void waitForElementToBeInVisible(EventFiringWebDriver driver,
			String element) {
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By
				.xpath(element)));
	}

	public void waitForElementToBeVisibleNew(EventFiringWebDriver driver,
			WebElement element) {
		wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	/**
	 * This method is to check whether page load is completed based on page
	 * title
	 * 
	 * @param driver
	 * @param title
	 */
	public static void waitForPageToLoad(EventFiringWebDriver driver) {
		String pageTitle, readyState;
		JavascriptExecutor js = null;
		do {
			js = (JavascriptExecutor) driver;
			readyState = (String) js
					.executeScript("return document.readyState");
			System.out.println("Status: " + readyState);
		} while (!readyState.equals("complete"));
	}

	public void homeMenuAndSubMenulinks(EventFiringWebDriver driver,
			WebElement eMainMenu, WebElement eSubMenu) {
		Actions action = new Actions(driver);
		action.moveToElement(eMainMenu).moveToElement(eSubMenu).click().build()
				.perform();
	}
	/**
	   * This method gets the newly created entities from application and verifies
	   * it with expected string.It returns the newly created entity name.
	   * 
	   * @param driver
	   * @param brandName
	   * @param xpath
	   * @return
	   */
	  public String verifyNewlyCreatedEntity(EventFiringWebDriver driver, String entityCreatedName) {
	   
	    String strActualText = null;
	    String strexpectedText = entityCreatedName;
	  
	    List<WebElement> list =
	        page_CommonObjects.list_proposals;
	    
	    Iterator<WebElement> iterator = list.iterator();
	    while (iterator.hasNext()) {
	      WebElement webElementList = iterator.next();
	      String strCurrenttext = webElementList.getText();
	      String[] spilts = strCurrenttext.split(": ");
	      strActualText = spilts[1];
	      if (strActualText.equals(strexpectedText)) {
	        break;
	      }

	    }
	    return strActualText;
	  }
	  public boolean isElePresent(EventFiringWebDriver driver, String element) {
			Boolean iselementpresent = driver.findElements(By.xpath(element)).size()!= 0;
			return iselementpresent;
		}
	  
	  public void selectPresentationDate(EventFiringWebDriver driver , String presentationDate) {
			String [] spilitDate = presentationDate.split("-");
			waitForElementToBeClickable(driver, page_CommonObjects.drop_presentationYear);
			if(!(getSelectedValueFromDropDown(page_CommonObjects.drop_presentationYear).getText().equals(spilitDate[2]))) {
				selectDataFromDropDown(driver, page_CommonObjects.drop_presentationYear, "selectElementByText", spilitDate[2]);
			}
			
			if(!(getSelectedValueFromDropDown(page_CommonObjects.drop_presentationMonth).getText().equals(spilitDate[1]))) {
				selectDataFromDropDown(driver, page_CommonObjects.drop_presentationMonth, "selectElementByText", spilitDate[1]);
			}
			
			if(getSelectedValueFromDropDown(page_CommonObjects.drop_presentationYear).getText().equals(spilitDate[2]) && 
					getSelectedValueFromDropDown(page_CommonObjects.drop_presentationMonth).getText().equals(spilitDate[1])) {
				Page_CommonObjects.Select_Date_From_DatePicker(driver, spilitDate[0]).click();
			}
			
			
			
		}
	  /**
	   * This method gets the newly created entities from application and verifies
	   * it with expected string.It returns the newly created entity name.
	   * 
	   * @param driver
	   * @param brandName
	   * @param xpath
	   * @return
	   */
	  public String verifyNewlyCreatedIOEntity(EventFiringWebDriver driver, String entityCreatedName) {
	   
	    String strActualText = null;
	    String strexpectedText = entityCreatedName;
	  
	    List<WebElement> list =
	        page_CommonObjects.list_IOs;
	    
	    Iterator<WebElement> iterator = list.iterator();
	    while (iterator.hasNext()) {
	      WebElement webElementList = iterator.next();
	      String strCurrenttext = webElementList.getText();
	      
	      strActualText = strCurrenttext;
	      if (strActualText.equals(strexpectedText)) {
	        break;
	      }

	    }
	    return strActualText;
	  }
	  
	  /**
	   * Method to assign custom dates provided into date format. Ex.28-January-2016
	   * (dd-mmmm-yyyy)
	   * 
	   * @param selectDate
	   *          Date into string format.
	*/
	public void selectCalenderDate(EventFiringWebDriver driver , String presentationDate) {
		String [] spilitDate = presentationDate.split("-");
		
		spilitDate[0] = Integer.valueOf(spilitDate[0]).toString();
		
		
		waitForElementToBeClickable(driver, page_CommonObjects.drop_presentationYear);
		
		if(!(getSelectedValueFromDropDown(page_CommonObjects.drop_presentationYear).getText().equals(spilitDate[2]))) {
			selectDataFromDropDown(driver, page_CommonObjects.drop_presentationYear, "selectElementByText", spilitDate[2]);
		}
		waitForElementToBeClickable(driver, page_CommonObjects.drop_presentationMonth);
		if(!(getSelectedValueFromDropDown(page_CommonObjects.drop_presentationMonth).getText().equals(spilitDate[1]))) {
			selectDataFromDropDown(driver, page_CommonObjects.drop_presentationMonth, "selectElementByText", spilitDate[1]);
		}
		
		if(getSelectedValueFromDropDown(page_CommonObjects.drop_presentationYear).getText().equals(spilitDate[2]) && 
				getSelectedValueFromDropDown(page_CommonObjects.drop_presentationMonth).getText().equals(spilitDate[1])) {
			this.waitForElementToBeVisible(driver, Page_CommonObjects.Select_Date_From_DatePickerOnIO(driver, spilitDate[0]));
			
			Page_CommonObjects.Select_Date_From_DatePickerOnIO(driver, spilitDate[0]).click();
		}
			
		
	}
	public void clickOnElementUsingJavascriptExecutor(EventFiringWebDriver driver,WebElement element){
		
		jsExecutor.executeScript(
		        "arguments[0].scrollIntoView(true);", element);
		jsExecutor.executeScript("arguments[0].click();", element);
		
		
	}
	
	public void waitForjQueryActive(EventFiringWebDriver driver) {
		int counter=0; 
		
		
		do {
			
			Object jQueryObject = ((JavascriptExecutor)
					driver).executeScript("return jQuery.active;");
					
					System.out.println(jQueryObject +":"+counter);
		if(jQueryObject.toString().equals("0")) {
			break;
		}
		} while(counter++ < 150);
		
	}
	
	public void waitForStaleElementExceptionToAvoid(EventFiringWebDriver driver, WebElement element) {
	    wait =
	        new WebDriverWait(driver, 360);
	    wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(element)));
	  }
	
	public void switchToNewWindow(EventFiringWebDriver driver, WebElement clickElement) {
		// Store the current window handle
		winHandleBefore = driver.getWindowHandle();

		// Perform the click operation that opens new window
		clickElement.click();
		
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		// Perform the actions on new window
	}
	
	public void switchToMainWindow(EventFiringWebDriver driver) {
		// Close the new window, if that window no more required
		driver.close();

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Continue with original browser (first window)
	}
	
	public void waitForElementToBeVisible(EventFiringWebDriver driver, WebElement visibleElement) {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOf(visibleElement));
	}
	
	public boolean isElementPresent(EventFiringWebDriver driver, By by) {
		  try {
		    driver.findElement(by);
		    return true;  // Success!
		  } catch (Exception ignored) {
		    return false;
		  }
	}
	
	public void avoidStaleElementException(EventFiringWebDriver driver, final WebElement ele) {
		new WebDriverWait(driver, 10)
	    .ignoring(StaleElementReferenceException.class)
	    .until(new Predicate<WebDriver>() {
	        @Override
	        public boolean apply(@Nullable WebDriver driver) {
	        	 ele.click();
	        	System.out.println("found element..");
	           
	            return true;
	        }
	    });
	}
	
	public void waitForPageLoad(EventFiringWebDriver driver)
	  {
	   
	   try{
	    Thread.sleep(1000);
	    int iWaitTime = 0;
	    boolean jQcondition = false;
	    while(true){
	     try{
	      if(driver != null){
	       
	        System.out.println("ReadyState=" + ((JavascriptExecutor) driver).executeScript("return document.readyState").toString());
	        jQcondition = (Boolean) ((JavascriptExecutor) driver)
	                      .executeScript("return window.jQuery != undefined && jQuery.active === 0");
	       
	        if(((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete") && jQcondition){
	        break;
	       }
	       else{
	        Thread.sleep(1000);
	        iWaitTime = iWaitTime + 1 ;
	        if(iWaitTime > 20000){
	        	System.out.println("Timeout Error while Page Load");
	         //throw new Exception("Timeout Error while Page Load");
	        }
	       }
	      }
	      else{
	       break;
	      }
	     }
	     catch(Exception e){
	      
	      break;
	     }
	    }
	    //Thread.sleep(1000);
	   }
	   catch(Exception e){
	    
	   }
	  }
	
}
