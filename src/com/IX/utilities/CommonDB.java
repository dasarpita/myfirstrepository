package com.IX.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class CommonDB {

	private static String dbusername;
	private static String dbpassword;
	Connection connection = null;
	Statement stmt;
	ResultSet  rs;
	//Should be defined as jdbc:mysql://host:port/database name
	private static String databaseURLQA= "jdbc:mysql://brandcdn-dev.cgy3zqnvq7mi.us-west-1.rds.amazonaws.com:3306/brandcdn_dev_ppr";
	private static String databaseURLSTAGE= "jdbc:mysql://brandcdn-stage-2015-09-27.cgy3zqnvq7mi.us-west-1.rds.amazonaws.com:3306/brandcdn_stage";
	private static String databaseURLPRODUCTION= "jdbc:mysql://prodhost:2020/easyDB";
	
	   
	public void setConnectionToDatabase(String testEnv) {
		String sqlQuery1= "INSERT INTO report_cached_adgroup_date VALUES ('3ggecjm', '2017-04-04', 100, 5, 0.00, 0, 0, 0, 0, '2017-04-04 20:59:16', '2017-04-04 20:59:16')";
		String connectionUrl="jdbc:mysql://server.brandcdndev.com:3306/";
        
        String resultValue = "";
        ResultSet rs;
        
        //To connect with QA Database
        if(testEnv.equalsIgnoreCase("QA")){
            connectionUrl = databaseURLQA;
            dbusername = "frequence_admin";
            dbpassword = "8#OB&AScBAmmM5Oe";
        }
        //To connect with Stage Database
        else if(testEnv.equalsIgnoreCase("STAGE")) {
            connectionUrl = databaseURLSTAGE;
            dbusername = "frequence_admin";
            dbpassword = "8#OB&AScBAmmM5Oe";
        }

        //To connect with Production Database
        else if(testEnv.equalsIgnoreCase("PRODUCTION")) {
            connectionUrl = databaseURLPRODUCTION;
            dbusername = "root";
            dbpassword = "prodpassword";
        }
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        try {
            connection = DriverManager.getConnection(connectionUrl,dbusername,dbpassword);
            if(connection!=null) {
                System.out.println("Connected to the database...");
            }else {
                System.out.println("Database connection failed to "+""+" Environment");
            }
            stmt = connection.createStatement();
           

           

        }catch(SQLException sqlEx) {
            System.out.println( "SQL Exception:" +sqlEx.getStackTrace());
        }
  }
	
	public void closeDataBaseConnection() {
		 try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public void insertDataIntoDatabaseTable(String sqlQuery) {
		
            try {
				stmt.executeUpdate(sqlQuery);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public ResultSet selectDataFromDatabaseTable(String sqlQuery) {
		
        try {
        	rs = stmt.executeQuery(sqlQuery);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
}
	
}
