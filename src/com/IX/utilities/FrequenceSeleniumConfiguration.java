package com.IX.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class FrequenceSeleniumConfiguration {


	 private static final FrequenceConfigurationConstants SELENIUM_CONFIG_CONSTANTS =
	      new FrequenceConfigurationConstants();
	 private static final Properties CONFIG_PROPERTIES = new Properties();
	 public final static String path= System.getProperty("user.dir");
		 
	
	 		
	 static {
		 File file = new File( path + "/prop.properties");
		 FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 try {
			CONFIG_PROPERTIES.load(fileInput);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	 /**
	   * Returns the value of the property corresponding to key.
	   * 
	   * @param key
	   *          the name of the property
	   * @return the value of the property
	   */
	  public static String getProperty(String key) {
	    return CONFIG_PROPERTIES.getProperty(key);
	  }
	/**
	 * Gets the Config Constants instance
	 * 
	 * @return SeleniumConfigConstants
	 */
	public static FrequenceConfigurationConstants getConfigConstants() {
	    return FrequenceSeleniumConfiguration.SELENIUM_CONFIG_CONSTANTS;
	}
	
}
