package com.IX.utilities;

import java.io.File;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;


public class SendStatusEmail {

	 public void sendMail(String reportFilePath) {	
		 
		 String MAILGUN_API_KEY = FrequenceSeleniumConfiguration.getConfigConstants().getMailApiKey();
         String MAILGUN_HOST = FrequenceSeleniumConfiguration.getConfigConstants().getMailHost(); // your host name

          Client client = Client.create();
          client.addFilter(new HTTPBasicAuthFilter("api", MAILGUN_API_KEY));

          WebResource webResource = client.resource("https://api.mailgun.net/v2/"
              + MAILGUN_HOST + "/messages");
          FormDataMultiPart formData = new FormDataMultiPart();
          formData.field("from", FrequenceSeleniumConfiguration.getConfigConstants().getMailFromAddress());
          formData.field("to", FrequenceSeleniumConfiguration.getConfigConstants().getMailToAddress());
          formData.field("bcc", FrequenceSeleniumConfiguration.getConfigConstants().getMailBccAddress());
          formData.field("subject", FrequenceSeleniumConfiguration.getConfigConstants().getMailSubject());
          formData.field("html", FrequenceSeleniumConfiguration.getConfigConstants().getMailBodyContents());
          formData.bodyPart(new FileDataBodyPart("attachment", new File("reportFilePath"), MediaType.TEXT_PLAIN_TYPE));
          ClientResponse clientResponse = webResource.type(MediaType.MULTIPART_FORM_DATA_TYPE)
              .post(ClientResponse.class, formData);
                  int status = clientResponse.getStatus();
               System.out.println("Status..." + status );
          if (status >= 400) {
            System.out.println(clientResponse.getEntity(String.class));
          }
	 }
}
