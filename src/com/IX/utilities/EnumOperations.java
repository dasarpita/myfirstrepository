package com.IX.utilities;

public class EnumOperations {


	  public enum WebElementLocatorEnum {
	    ById, ByXpath, ByName, ByClassName, ByLinkText, ByPartialLinkText, ByCssSelector
	  };
	  
	  public enum SelectFeatureForPartner {
		  AccessManager,Accounts,AdWorksGallery,AdWorksUploader,Advertiser_Group,CreateAdvertiser,CreateHelpdeskTicket,
		  CreateMOATTag,Creatives,DemoPartner,EditAdvertisers,Gallery,GalleryBuilder,InsertionOrders,InventoryManager,
		  IoSubmitButton,LiftReport,NewInsertionOrderLink,Partners,ProposalBuilder,Proposals,RealPartner,
		  RegisterAdvertiserUsers,ReportHeatmap,Reports,ReportsDisplayCreatives,ReportsExpandedTVReporting,ReportsPrintButton,
		  ReportsTMPi,RFPBuilderAddSlides,RFPBuilderDragSlides,RFPBuilderRemoveSlides,SalesActivityReporting,SampleAdBuilder,
		  SampleAdManager,UserEditor
		  };

}
