package com.IX.utilities;

import org.apache.log4j.Logger;

public class Log {
  /* Initialize Log4j logs */
  public static Logger LogService = Logger.getLogger(Logger.class.getName());

  /**
   * @param strTestCaseName
   * 
   *          It will Print the log for beginning of test case.
   * */
  public static void startTestCase(String strTestCaseName) {
    LogService
        .info("*********************************************************************************");
    LogService.info("###################   " + strTestCaseName + " TC START "
        + "    #################");
  }

  /**
   * @param strTestCaseName
   * 
   *          It will Print the log for Ending of test case.
   * */
  public static void endTestCase(String strTestCaseName) {
    LogService
        .info("*********************************************************************************");
    LogService.info("XXXXXXXXXXXXXXXXXXX   " + strTestCaseName + " TC END"
        + "    XXXXXXXXXXXXXXXXXXXXX");
  }

  public static void info(String strMessage) {
    LogService.info(strMessage);
  }

  public static void warn(String strMessage) {
    LogService.warn(strMessage);
  }

  public static void error(String message) {
    LogService.error(message);
  }

}
