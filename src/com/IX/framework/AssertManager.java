package com.IX.framework;


import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

/**
 * This is Assert Manager class. This will be used for Custom assert or soft
 * assert.
 * 
 * */
public class AssertManager {
	
	  public static StringBuffer verificationErrors = new StringBuffer();
	  public static SoftAssert softAssert = new SoftAssert();
  /**
   * Parameter less constructor.
   */
  public AssertManager() {

  }

  /**
   * Verify text String
   * 
   * @param String
   * @param Boolean
   * 
   * */
  public void verifyTrue(String strMsg, Boolean bStatus) {
    try {
      Assert.assertTrue(bStatus.booleanValue());
    } catch (Error e) {
      Reporter.log(strMsg + e.getMessage() + "<br>");
    }
  }

  /**
   * Verify Two String
   * 
   * @param String
   *          - Text message to be displayed if fail
   * @param String
   *          - Expected String text
   * @param String
   *          - Actual String text
   * */
  public void verifyTextEquals(String strExpected, String strActual, String strMsg) {
    try {
    	Assert.assertEquals(strExpected, strActual);
    } catch (Error e) {
    	verificationErrors.append(e.toString()); 
    	Reporter.log(strMsg +e.getMessage());
    }
    
  }
  
  public void verifyAssertEquals(double valActual, double valExpected, String strMsg) {
	    try {
	      Assert.assertEquals(valActual, valExpected, strMsg);
	    } catch (Error e) {
	    verificationErrors.append(e.toString()); 
	      Reporter.log(strMsg + e.getMessage() + "<br>");
	    }
	  }
  
  public void verifyAssertEquals(long valActual, long valExpected, String strMsg) {
	    try {
	      Assert.assertEquals(valActual, valExpected, strMsg);
	    } catch (Error e) {
	    verificationErrors.append(e.toString()); 
	      Reporter.log(strMsg + e.getMessage() + "<br>");
	    }
	  }
  
  public void verifyAssertEquals(int valActual, int valExpected, String strMsg) {
	    try {
	      Assert.assertEquals(valActual, valExpected, strMsg);
	    } catch (Error e) {
	    verificationErrors.append(e.toString()); 
	      Reporter.log(strMsg + e.getMessage() + "<br>");
	    }
	  }

}
