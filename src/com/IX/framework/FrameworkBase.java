package com.IX.framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
//import com.IX.pages.PagesWrapper;
import com.IX.listeners.TestEventHandler;

import com.IX.utilities.CommonKeywords;

public class FrameworkBase {

	public WebDriver wbdriver;
	public EventFiringWebDriver driver;
	public final static String path= System.getProperty("user.dir");
	public static Properties prop = new Properties();	
	public static Logger log= Logger.getLogger(Driver.class);
	public static TestEventHandler eventListener;
	
	//public static ExtentReports extent;
	//public static ExtentTest reportTest;
	//public static PagesWrapper pagesWrapper;
	public static CommonKeywords keywords;
	public static int timeout;
	public static final String ESCAPE_PROPERTY = "org.uncommons.reportng.escape-output"; 
	public static Date date = new Date();
	String dynamicEnvURL = System.getProperty("envURL");
	
	
/*	public void setReports (Method m){
		Test testAnnotation = m.getAnnotation(Test.class); //getting the testcase description for showing on the reports. 
		reportTest = extent.startTest(m.getName(),testAnnotation.description());
		String[] group= testAnnotation.groups();
		reportTest.assignCategory(group);
	}*/
		
	@DataProvider(name = "TestDataFile")
	public static Object[][] readData(Method m) throws Exception {
		File folder= new File(path + "/testdata");
		File[] filesInFolder = folder.listFiles();
		String[][] retObjArr=null;
		boolean flag = false;
		
			try{
			 	for (int numberOfFilesInFolder = 0; numberOfFilesInFolder < filesInFolder.length && flag == false; numberOfFilesInFolder++) {
			 	String filePath = filesInFolder[numberOfFilesInFolder].toString();
			 		FileInputStream file= new FileInputStream(new File(filePath));
			 		Workbook workbookb = WorkbookFactory.create(file);
			 		for(int sheetIndex=0; sheetIndex< workbookb.getNumberOfSheets(); sheetIndex++){
			 			String sheetNames = workbookb.getSheetName(sheetIndex);
				 		if(sheetNames.equals(m.getDeclaringClass().getSimpleName())){
				 			retObjArr = loadExcelFile(workbookb,m.getDeclaringClass().getSimpleName());
				 			flag= true;
				 			break;
				 		}
			 		}
				 }
			 	if(flag == false)
			 		throw new NullPointerException();
			}
			catch(Exception e){
				System.out.println(e);
				log.info(e.getMessage() + " Datasheet for required testcase not found");
				Reporter.log(e.getMessage() + " Datasheet for required testcase not found");
			} 
		return (retObjArr);
	}

	public static String[][] loadExcelFile(Workbook workbook, String sheetName) throws IOException{
		int rowCount, columnCount;
		Sheet sheet  = workbook.getSheet(sheetName);
		rowCount = sheet.getLastRowNum();
		columnCount = sheet.getRow(0).getLastCellNum();
		String[][] tempArray = new String[rowCount][columnCount]; //rowCount-1 to leave reading 1st row i.e column heading
		try{
			for (int i=1; i<=rowCount; i++){
    			Row row = sheet.getRow(i);
    				for (int j=0; j<columnCount; j++){
    				Cell cell= row.getCell(j); 
    					switch (cell.getCellType()) {
	                    	case Cell.CELL_TYPE_STRING:
		                        System.out.print(cell.getStringCellValue());
		                        String str= cell.getStringCellValue();		                        
		                        tempArray[i-1][j]= str; 
		                        break;	
    					}
    				}
    			}	
		}
		catch(Exception e){
			System.out.println(e);
			log.info(e.getMessage());
		}
		return tempArray;
	}

	public static Properties readProp()	{
		InputStream input = null;
			try {
				String filename = (path + "/prop.properties");
				input = new FileInputStream(filename);
				prop.load(input);
    	    }
			catch(IOException e){
				e.printStackTrace();
				log.warn(e.getMessage());//added for logging
			}
		return prop;
	}
	
	public static String readPropValue(String prop)	{
		String propertyValue=null;
			try {
				propertyValue=readProp().getProperty(prop).toLowerCase();
				if (propertyValue.isEmpty()){
					throw new NullPointerException();
				}
			}
			catch(Exception e){
				log.info(e.getMessage() + " for property: " + prop);//added for logging
			}
		return propertyValue;
	}
	
	 /**
	 * @param m
	 * @throws IOException
	 */
	public void takeScreenshots(String m) throws IOException{
		try{
			File scrFile = ((TakesScreenshot)wbdriver).getScreenshotAs(OutputType.FILE);
			String screenshotPath= path + "/reports/screenshots/" + "Build No-" + readProp().getProperty("BuildNumber") + "-" + getCurDate() + "/" + m + "-" + getCurTime() + ".png";
			FileUtils.copyFile(scrFile, new File(screenshotPath));
			//FrameworkBase.reportTest.log(LogStatus.INFO,"Error Snapshot below: " + FrameworkBase.reportTest.addScreenCapture(screenshotPath));
		}
		catch(IOException e){
			e.printStackTrace();
			log.info(e.getMessage());//added for logging
		}
	}
	
	public static String getCurTime(){
		DateFormat curTime;
		curTime = new SimpleDateFormat("h-mm-ss");
		return curTime.format(date);
	}
	
	public static String getCurDate(){
		DateFormat curDate;
		curDate = new SimpleDateFormat("yyyy-MM-dd");
		return curDate.format(date);
	}

}

