package com.IX.framework;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.log4j.Logger;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import com.IX.listeners.TestEventHandler;
import com.IX.utilities.CommonKeywords;
/**
 * Driver class invokes the required browser as mentioned in the prop.properties
 * @author 
 *
 */

public class Driver extends FrameworkBase {

public static String reportpath;

@BeforeSuite(groups = "")
public void suiteSetup(){
	reportpath = path + "/test-output" + "/frequence_automation_report.html";
}

@Parameters({"ApplicationURL"})
@BeforeMethod(groups = "")
	public EventFiringWebDriver setup(String ApplicationURL, Method m){
		try{
			System.out.println("App URL:" + ApplicationURL);
			//setReports(m);
			log = Logger.getLogger(Driver.class);
			driver= invokeBrowser((readProp().getProperty("Browser")).toLowerCase(), ApplicationURL);
			log.info("---Browser started---");
			keywords=new CommonKeywords();
		}
		catch(Exception e){
			System.out.println(e);
		}
	return driver;
	}


public EventFiringWebDriver invokeBrowser(String Browser, String ApplicationURL){	
	try{
		
	// Xvfb required parameters like screen with, screen height and display number  
		int width = Integer.parseInt(prop.getProperty("SCREENSHOT_WIDTH"));
		int height = Integer.parseInt(prop.getProperty("SCREENSHOT_HEIGHT"));
		int display_number  = Integer.parseInt(prop.getProperty("XVFB_DISPLAY"));
		String xvfb_screen_size = (width)+"x"+(height)+"x"+"24";
		
		System.out.println(display_number + "-" + xvfb_screen_size);
		System.out.println(prop.getProperty("XVFB_PATH"));  
		
		
		switch(Browser)	{ 
			case "firefox": 
				wbdriver = new FirefoxDriver();
				break;
			case "ie":
				DesiredCapabilities cp = DesiredCapabilities.internetExplorer();	
				cp.setJavascriptEnabled(true);
				cp.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				System.setProperty("webdriver.ie.driver",path + "/jars/IEDriverServer_32.exe");
				wbdriver = new InternetExplorerDriver(cp);
				break;
			case "chrome":
				
				// to run chrome browser on actual screen 
				
				System.setProperty("webdriver.chrome.driver",path + "/jars/chromedriver.exe");
				wbdriver = new ChromeDriver();
				
				break;
				
		}
		wbdriver.manage().window().maximize();
		driver = new EventFiringWebDriver(wbdriver);
		eventListener = new TestEventHandler(wbdriver);
		driver.register(eventListener);
		System.out.println("Driver Object:" + driver);
		//driver.manage().timeouts().implicitlyWait(Integer.parseInt((readProp().getProperty("Timeout"))), TimeUnit.SECONDS);//reading from property
		//driver.manage().window().maximize();
		//FrameworkBase.reportTest.log(LogStatus.PASS, Browser + " browser openened sucessfully");
		try {
			driver.get((readProp().getProperty(ApplicationURL)).toLowerCase());
		} catch(Exception e) {
			System.out.println(e);
		}
		System.out.println("URL passed by command line " + ApplicationURL);
		//driver.get(dynamicEnvURL);
		//driver.get(ApplicationURL);
		//driver.manage().window().setSize(new Dimension(width,height));
	}
	catch(Exception e){
		System.out.println(e);
	}
	return driver;
}
	
	@AfterMethod(groups = "")
	public void tearDown() {
		System.out.println("Closing..");
		driver.close();
		driver.quit();
		//extent.endTest(reportTest);
	}

	@AfterSuite(groups = "")
	public void endSuite(){
		System.out.println("In After suite");
		//driver.quit();
		//wbdriver.quit();
		Map<String, String> sysInfo = new HashMap<String, String>();
		sysInfo.put("Selenium Version", "2.46"); //information to be displayed on reports
		sysInfo.put("Environment", "Prod");
		//extent.addSystemInfo(sysInfo);
	}
	
/*
 * function run_script()
 * This function is to run commands to open Xvfb screen
 * @param String command
 * @return void
 */

public static void run_script(String command)
{
	CommandLine oCmdLine = CommandLine.parse(command);
    DefaultExecutor oDefaultExecutor = new DefaultExecutor();
    oDefaultExecutor.setExitValue(0);
    try
    {
        oDefaultExecutor.execute(oCmdLine);
    } 
    catch (ExecuteException e) 
    {
        e.printStackTrace();
    } 
    catch (IOException e) 
    {
        e.printStackTrace();
    }
}
	
}
	

