package com.IX.framework;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import org.testng.annotations.Test;


public class TestEngine {

	static FrameworkBase fb = new FrameworkBase();
	static String className = TestEngine.class.getName();
	static String envURL = FrameworkBase.readProp().getProperty("URL1").toLowerCase();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();
		//System.out.println("Hello World");
	}
	
	public static void run() {
		List<XmlSuite> suites = new ArrayList<XmlSuite>(); 
		XmlSuite suite=new XmlSuite();
		List<XmlClass> classes =new ArrayList<XmlClass>();
		List<Class> listnerClasses = new ArrayList<Class>();
		
		listnerClasses.add(com.IX.listeners.CustomTestListenerAdapter.class);
	    listnerClasses.add(com.IX.listeners.ExtentReporterNG.class); 
		
		Class[] allModuleTestCaseClasses =
		         getClasses((className.substring(0, className.lastIndexOf("framework") - 1)).trim()
		         + ".tests");
		
		for(Class allModuleTestCaseClasse:allModuleTestCaseClasses){
			System.out.println(allModuleTestCaseClasse.toString());
			Method [] methods=allModuleTestCaseClasse.getDeclaredMethods();
			
			for(Method m:methods){
				System.out.println(m.toString());
				Annotation ans = m.getAnnotation(org.testng.annotations.Test.class);
				System.out.println(ans);
				if(ans!=null){
					try {
					String group=((Test) ans).groups()[0];
					System.out.println("Gr"+group);
					} catch(Exception e) {
						
					}
				}
			}
			classes.add(new XmlClass(allModuleTestCaseClasse));
		}
		List<String> groupList=new ArrayList<String>();
		 System.out.println("envUR---L"+envURL);
         System.out.println("DynUR---L"+fb.dynamicEnvURL);
         if(fb.dynamicEnvURL!=null) {
          if(fb.dynamicEnvURL.contains("brandcdndev")) {
                 groupList.add("");
                 groupList.add("dev");
         } else if(fb.dynamicEnvURL.contains("brandcdnstage")) {
                 groupList.add("");
                 groupList.add("stage");
         } else if(fb.dynamicEnvURL.contains("brandcdn")) {
                 groupList.add("");
                 groupList.add("prod");
  }
} else {
         if(envURL.contains("brandcdndev")) {
                 groupList.add("");
                 groupList.add("dev");
                 System.out.println("in Dev");
         } else if(envURL.contains("brandcdnstage")) {
                 groupList.add("");
                 groupList.add("stage");
                 System.out.println("in Stage");
         } else if(envURL.contains("brandcdn")) {
                 groupList.add("");
                 groupList.add("prod");
                 System.out.println("in Prod");
         }
 }
         XmlTest xmlTest=new XmlTest(suite);
         xmlTest.setXmlClasses(classes);
         System.out.println(fb.dynamicEnvURL);
         if(fb.dynamicEnvURL!=null) {
                 Map<String,String> testngParams = new HashMap<String,String> ();
                 testngParams.put("ApplicationURL", fb.dynamicEnvURL);
                 xmlTest.setParameters(testngParams);
         }

         else {
             Map<String,String> testngParams = new HashMap<String,String> ();
             testngParams.put("ApplicationURL", "URL1");
             xmlTest.setParameters(testngParams);
     }

		
		xmlTest.setXmlClasses(classes);
		xmlTest.setIncludedGroups(groupList);
		
		suites.add(suite);  
		System.out.println(suite.toXml());
		TestNG tng = new TestNG();
		tng.setXmlSuites(suites);
		tng.setListenerClasses(listnerClasses);
		System.out.println(suite.toXml());
		tng.run();
		System.out.println("After Test NG");
	}
	
	private static Class[] getClasses(String packageName) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = null;
		try {
			resources = classLoader.getResources(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements())
        {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<Class>();
        for (File directory : dirs)
        {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }

	private static List<Class> findClasses(File directory, String packageName) {
        List<Class> classes = new ArrayList<Class>();
        if (!directory.exists())
        {
            System.out.println("Directory does not exist");
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files)
        {
            if (file.isDirectory())
            {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            }
            else if (file.getName().endsWith(".class"))
            {
                try {
					classes.add(Class.forName(packageName
					                          + '.'
					                          + file.getName()
					                              .substring(0, file.getName().length() - 6)));
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
        return classes;
    }

}
